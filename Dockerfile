FROM node:16-alpine

WORKDIR /usr/src/app

COPY app.js /usr/src/app
EXPOSE 3001
CMD node app.js
